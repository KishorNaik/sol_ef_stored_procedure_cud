﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Stored_Procedure_CUD.Entity
{
    public class UserEntity
    {
        public decimal UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}
